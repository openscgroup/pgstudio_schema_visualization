package com.openscg.pgstudio.server.models;

import java.io.File;
import java.sql.Connection;

import net.sourceforge.schemaspy.Main;

import org.apache.commons.io.FileUtils;


public class VisualizeSchemaData {
	private final Connection conn;

	public VisualizeSchemaData(Connection conn) {
		this.conn = conn;
	}

	public String generateSchemaData(String schemaName, String databaseName) {
		Main schemaSpyClient = new Main();
		int returnCode = 0;
		String[] schemaArgs = {"-t=pgsql", "-db="+databaseName, "-s="+schemaName, "-o=../webapps/pgstudio/images/schema","-gv=../webapps/pgstudio/WEB-INF/lib/graphviz/2.38.0/"};
		try{
			deleteExistingImagesDir();
			returnCode = schemaSpyClient.main(schemaArgs, conn);
			System.out.println("generate schema execution is done******returncode="+returnCode);
			conn.close();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return Integer.toString(returnCode);
	}

	private void deleteExistingImagesDir() {
		File index = new File("../webapps/pgstudio/images/schema");
		if (index.exists()) {
			System.out.println("existed and deleted.........");
			try {
				FileUtils.deleteDirectory(index);
			} catch(Exception ex) {
				System.out.println("error while deleting directory");
			}
		} else {
			System.out.println("not existed ...............");
		}
	}

}
