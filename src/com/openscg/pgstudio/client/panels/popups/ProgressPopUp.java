package com.openscg.pgstudio.client.panels.popups;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.openscg.pgstudio.client.PgStudio;

public class ProgressPopUp implements StudioPopUp{
	final DialogBox dialogBox = new DialogBox();
	String msg;
	Image image = new Image(PgStudio.Images.displayProcessingImage());
	Button okBtn ;
	Label label;
	VerticalPanel panel ;
	String displayCode ="";
	@Override
	public DialogBox getDialogBox() throws PopUpException {

		dialogBox.setGlassEnabled(true);
		dialogBox.center();
		return dialogBox;
	}


	public void setMessage(String str) {
		if(displayCode.equalsIgnoreCase("progress")) {
			dialogBox.remove(image);
		}
		dialogBox.remove(image);
		dialogBox.setVisible(true);
		dialogBox.setGlassEnabled(true);
		panel = new VerticalPanel();
		panel.setHeight("100");
		panel.setWidth("300");
		panel.setSpacing(10);
		panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		label = new Label(str);

		//dialogBox.add(label);
		okBtn = new Button("OK") ;

		okBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		});
		panel.add(label);
		panel.add(okBtn);
		displayCode = "text";
		dialogBox.add(panel.asWidget());
		dialogBox.center();

	}
	
	public void setProgressImage() {
		if(displayCode.equalsIgnoreCase("text")) {
			dialogBox.remove(panel);
		}
		dialogBox.add(image.asWidget());
		displayCode = "progress";
	}

	public void hide() {
		if(displayCode.equalsIgnoreCase("text")) {
			dialogBox.remove(panel);
		} else if(displayCode.equalsIgnoreCase("progress")) {
			dialogBox.remove(image);
		}
		dialogBox.hide(true);

	}

	public Widget getButton() {

		okBtn = new Button("OK") ;

		okBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		});


		return okBtn.asWidget();
	}


}
