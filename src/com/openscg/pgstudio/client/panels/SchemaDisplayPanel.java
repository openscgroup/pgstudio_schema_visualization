package com.openscg.pgstudio.client.panels;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class SchemaDisplayPanel extends Composite {
public SchemaDisplayPanel(String imageData) {
		
		VerticalPanel panel = new VerticalPanel();
		panel.setWidth("1200px");
		panel.setHeight("600px");
		Frame frm = new Frame("http://localhost:8080/pgstudio/images/schema/index.html");
		frm.setWidth("1200px");
		frm.setHeight("1200px");
		panel.add(frm);
		initWidget(panel);
	}


private Widget getProfilePicture(String imageData) {
	 Image profilePicture = new Image();     
	 String styleName = profilePicture.getStyleName();
	 profilePicture.setUrl(imageData);
	 profilePicture.setStyleName(styleName);
	 return profilePicture.asWidget();
	}

}
